// C++ code abrir puerta portal


int PIN_CODIGO1 = 2;
int PIN_CODIGO2 = 3;
int PIN_CODIGO3 = 4;
int PIN_CODIGO4 = 5;
int PIN_LED = 10;
int PIN_PULSADOR = 13;
// definimos la valor de la pulsacion larga
double PULSACION_LARGA = 300;

int botonPulsado = 0;
// codigo fijado en el conmutador
int codigo1 = 0;
int codigo2 = 0;
int codigo3 = 0;
int codigo4 = 0;
// codigo de lectura mediante pulsacion
// el valor 5 nos indica que no hay pulsacion
// pulsacion corta almacenara 0
// pulsacion larga almacenara 1
int pulsacion1 = 5;
int pulsacion2 = 5;
int pulsacion3 = 5;
int pulsacion4 = 5;
// variables para controlar el tiempo
double cronometro = 0;
double tiempoInicioPulsacion = 0;
double duracionPulsacion = 0;
// variable que nos indica si abrimos la puerta
int abrirPuerta = 0;


void setup()
{
  // definimos los pins de entrada y salida
  pinMode(PIN_LED, OUTPUT);
  pinMode(PIN_PULSADOR, INPUT);
  pinMode(PIN_CODIGO1, INPUT);
  pinMode(PIN_CODIGO2, INPUT);
  pinMode(PIN_CODIGO3, INPUT);
  pinMode(PIN_CODIGO4, INPUT);
  
  // definimos el modo serie para contar
  Serial.begin(9600);
}

void loop()
{
  // leemos si esta pulsado el boton
  botonPulsado = digitalRead(PIN_PULSADOR);
  // leemos nuestro codigo secreto los 4 digitos
  codigo1 = digitalRead(PIN_CODIGO1);
  codigo2 = digitalRead(PIN_CODIGO2);
  codigo3 = digitalRead(PIN_CODIGO3);
  codigo4 = digitalRead(PIN_CODIGO4);
  
  // contamos en milisegundos con el cronometro
  cronometro = millis();
    
  // si el boton esta pulsado encendemos el led
  if (botonPulsado == HIGH) {
    // marcamos cuando empezamos a pulsar el pulsador
    if (tiempoInicioPulsacion == 0){
      tiempoInicioPulsacion = cronometro;
    }
    // encendemos el led
    digitalWrite(PIN_LED, HIGH);
  } else {
    // cuando hemos pulsado el pulsador empezamos a contar
    if (tiempoInicioPulsacion != 0) {
      while (botonPulsado == HIGH){
        // dejo pasar el tiempo
      }
      // duracion de la pulsacion
      duracionPulsacion = cronometro - tiempoInicioPulsacion;
      tiempoInicioPulsacion = 0;
      // si no esta pulsado apagamos el led
      digitalWrite(PIN_LED, LOW);
      Serial.print(String("Han transcurrido "));
      Serial.print(duracionPulsacion);
      Serial.print(String(" milisegundos desde que has pulsado"));
      crearCodigoPulsaciones();
      // si tenemos definido nuestro codigo
      // verificaoms si es el guardado en el conmutador
      if( (pulsacion1 != 5) && (pulsacion2 != 5) &&
         (pulsacion3 != 5) && (pulsacion4 != 5) ) {
        // comprobamos si el codigo es correcto
        abrirPuerta = verificaCodigo();
        //reseteo los codigos pulsados
        pulsacion1 = 5;
        pulsacion2 = 5;
        pulsacion3 = 5;
        pulsacion4 = 5;
        // nos aseguramos que el led esta apagado
        digitalWrite(PIN_LED, LOW);
        delay(500);
        if (abrirPuerta == 1){
          ledCodigoCorrecto();
          // TODO abrir puerta
        }else {
          ledCodigoIncorrecto();
        }
      }
    } 
  }
  delay(100); 
}

// muestra que el codigo es CORRECTO
// parpadeo largo de 3 segundos
void ledCodigoCorrecto(){
  digitalWrite(PIN_LED, HIGH);
  delay(3000);
  digitalWrite(PIN_LED, LOW);
}

// muestra que el codigo es INCORRECTO
// mediate parpadeos del led
void ledCodigoIncorrecto(){
  digitalWrite(PIN_LED, HIGH);
  delay(300);
  digitalWrite(PIN_LED, LOW);
  delay(300);
  digitalWrite(PIN_LED, HIGH);
  delay(300);
  digitalWrite(PIN_LED, LOW);
  delay(300);
  digitalWrite(PIN_LED, HIGH);
  delay(300);
  digitalWrite(PIN_LED, LOW);
  delay(300);
  digitalWrite(PIN_LED, HIGH);
  delay(300);
  digitalWrite(PIN_LED, LOW);
  delay(300);
  digitalWrite(PIN_LED, HIGH);
  delay(300);
  digitalWrite(PIN_LED, LOW);
}

// vamos a ir rellenando los codigos de las pulsaciones
void crearCodigoPulsaciones(){
  int codigo = 5;
  
  // primero calculamos si es una pulsacion larga o corta
  if (duracionPulsacion < PULSACION_LARGA) {
    codigo = 0;
  } else {
    codigo = 1;
  }
  // si no esta definido el codigo1, codigo2 ... lo definimos
  if (pulsacion1 == 5){
    pulsacion1 = codigo;
  } else {
    if (pulsacion2 == 5){
      pulsacion2 = codigo;
    } else {
      if (pulsacion3 == 5){
        pulsacion3 = codigo;
      } else {
        if (pulsacion4 == 5){
          pulsacion4 = codigo;
        }
      }
    }
  }
}

// devuelvo 0 si el codigo es incorrecto
// devuelvo 1 si el codigo es correcto
int verificaCodigo(){
  return ( (codigo1 == pulsacion1) &&
          (codigo2 == pulsacion2) &&
          (codigo3 == pulsacion3) &&
          (codigo4 == pulsacion4) );
}
